import React, { FC } from 'react'
import 'fontsource-roboto'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navbar from './components/Navbar/Navbar'
import Authorization from './components/Authorization/Authorization'
import { Meetroom } from './components/Meetroom/Meetroom'

const App: FC = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <div className="container">
        <Switch>
          <Route component={Authorization} path="/" exact />
          <Route component={Meetroom} path="/meetroom" />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

export default App
